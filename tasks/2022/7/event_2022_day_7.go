package main

import (
	"flag"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

var RegExpFile = regexp.MustCompile(`^(\d+) ([\w\.]+)`)
var RegExpDir = regexp.MustCompile(`^dir (\w+)`)
var RegExpCmd = regexp.MustCompile(`^\$ (\w+) ?([\w/\.]*)`)

type File struct {
	Raw  string
	Name string
	Size int
}

func (f *File) Parse() (err error) {
	r := RegExpFile.FindStringSubmatch(f.Raw)

	f.Size, err = strconv.Atoi(r[1])
	if err != nil {
		return
	}

	f.Name = r[2]

	return
}

func (f File) String() string {
	return fmt.Sprintf("* %s (%d)", f.Name, f.Size)
}

type Dir struct {
	Raw     string
	Name    string
	Parent  *Dir
	Files   map[string]*File
	SubDirs map[string]*Dir
}

func (d *Dir) Parse() (err error) {
	r := RegExpDir.FindStringSubmatch(d.Raw)
	d.Name = r[1]
	return
}
func (d *Dir) AddFile(file *File) {
	d.Files[file.Name] = file
}

func (d *Dir) AddDir(dir *Dir) {
	d.SubDirs[dir.Name] = dir
}

func (d Dir) String() string {
	return fmt.Sprintf("- %s (%d)", d.Name, d.SizeRecursive())
}

func (d Dir) StringRecursive(depth int) string {
	var indent string
	for i := 0; i < depth; i++ {
		indent += "\t"
	}
	result := fmt.Sprintf("%s%s\n", indent, d.String())
	for _, subdir := range d.SubDirs {
		result += subdir.StringRecursive(depth + 1)
	}
	for _, file := range d.Files {
		result += fmt.Sprintf("%s\t%s\n", indent, file.String())
	}
	return result
}

func (d Dir) SizeRecursive() int {
	var result int
	for _, subdir := range d.SubDirs {
		result += subdir.SizeRecursive()
	}
	for _, file := range d.Files {
		result += file.Size
	}
	return result
}

func (d Dir) GetDirsWithMaxSize(size int) []*Dir {
	result := make([]*Dir, 0)
	if d.SizeRecursive() <= size {
		result = append(result, &d)
	}
	for _, subdir := range d.SubDirs {
		result = append(result, subdir.GetDirsWithMaxSize(size)...)
	}
	return result
}

func (d Dir) FindSmallestDirWithMinSize(size int) *Dir {
	var result *Dir
	if d.SizeRecursive() > size {
		result = &d
	}

	for _, subdir := range d.SubDirs {
		r := subdir.FindSmallestDirWithMinSize(size)
		if r != nil {
			if result != nil {
				if r.SizeRecursive() < result.SizeRecursive() {
					result = r
				}
			} else {
				result = r
			}
		}
	}
	return result
}

type Cmd struct {
	Raw     string
	Command string
	Args    string
}

func (c *Cmd) Parse() (err error) {
	r := RegExpCmd.FindStringSubmatch(c.Raw)
	c.Command = r[1]
	c.Args = r[2]
	return
}

type Parser struct {
	InputLines []string
	Tree       *Dir
}

func (p *Parser) GetLineType(line string) (lineType string) {

	if RegExpCmd.MatchString(line) {
		lineType = "cmd"
	} else if RegExpDir.MatchString(line) {
		lineType = "dir"
	} else if RegExpFile.MatchString(line) {
		lineType = "file"
	}

	return
}

func (p *Parser) Parse() (err error) {

	root := NewDir(nil)
	root.Name = "/"

	cur := root

	for _, line := range p.InputLines {
		if len(line) == 0 {
			continue
		}
		switch p.GetLineType(line) {
		case "cmd":
			log.Printf("cur: %s, parent: %s, found cmd: %s", cur, cur.Parent, line)
			var cmd *Cmd
			cmd, err = NewCmdFromLine(line)
			if err != nil {
				return
			}
			log.Printf("parsed command: %s, args: %#v", cmd.Command, cmd.Args)
			switch cmd.Command {
			case "cd":
				switch cmd.Args {
				case "/":
					continue
				case "..":
					log.Println("moving up one level")
					if cur.Parent != nil {
						cur = cur.Parent
					}
				default:
					cur = cur.SubDirs[cmd.Args]
				}
			case "ls":
				continue
			default:
				err = fmt.Errorf("unknown command: %s", cmd.Command)
			}
		case "dir":
			log.Printf("cur: %s, parent: %s, found dir: %s", cur, cur.Parent, line)
			var dir *Dir
			dir, err = NewDirFromLine(cur, line)
			if err != nil {
				return
			}
			cur.AddDir(dir)
		case "file":
			log.Printf("cur: %s, parent: %s, found file: %s", cur, cur.Parent, line)
			var file *File
			file, err = NewFileFromLine(line)
			if err != nil {
				return
			}
			cur.AddFile(file)
		default:
			err = fmt.Errorf("failed parsing line: %s", line)
		}
	}

	p.Tree = root

	return
}

func NewFileFromLine(line string) (f *File, err error) {
	f = &File{
		Raw: line,
	}
	err = f.Parse()
	return
}

func NewDir(parent *Dir) *Dir {
	files := make(map[string]*File, 0)
	dirs := make(map[string]*Dir, 0)
	return &Dir{
		Parent:  parent,
		Files:   files,
		SubDirs: dirs,
	}
}

func NewDirFromLine(parent *Dir, line string) (d *Dir, err error) {
	d = NewDir(parent)
	d.Raw = line
	err = d.Parse()
	return
}

func NewCmdFromLine(line string) (c *Cmd, err error) {
	c = &Cmd{
		Raw: line,
	}
	err = c.Parse()
	return
}

func Event2022Day7Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	p := Parser{
		InputLines: strings.Split(task.Input, "\n"),
	}
	err := p.Parse()
	if err != nil {
		log.Fatalf("parsing failed: %s", err.Error())
	}

	fmt.Println(p.Tree.StringRecursive(0))

	r := p.Tree.GetDirsWithMaxSize(100000)

	var total int
	for _, dir := range r {
		total += dir.SizeRecursive()
	}

	return total, nil
}

func Event2022Day7Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	p := Parser{
		InputLines: strings.Split(task.Input, "\n"),
	}
	err := p.Parse()
	if err != nil {
		log.Fatalf("parsing failed: %s", err.Error())
	}

	fmt.Println(p.Tree.StringRecursive(0))

	needed := 30000000 - (70000000 - p.Tree.SizeRecursive())

	r := p.Tree.FindSmallestDirWithMinSize(needed).SizeRecursive()

	return r, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 7)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day7Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day7Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
