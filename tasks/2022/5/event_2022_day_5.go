package main

import (
	"flag"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Stacks [][]string

func (s Stacks) Move(src, dst int) {
	s[dst] = append([]string{s[src][0]}, s[dst]...)
	s[src] = s[src][1:]
}

func (s Stacks) MoveNum(num, src, dst int) {
	for i := 0; i < num; i++ {
		s.Move(src, dst)
	}
}

func (s Stacks) MoveNum9001(num, src, dst int) {

	fmt.Printf("Moving %d from %d to %d\n", num, src, dst)
	fmt.Println(s)

	m := make([]string, 0)
	for _, i := range s[src][0:num] {
		m = append(m, i)
	}
	fmt.Printf("m: %#v\n", m)

	r := s[src][num:]
	fmt.Printf("r: %#v\n", r)

	fmt.Printf("r: %#v\n", r)
	fmt.Println(s)
	s[src] = r

	fmt.Printf("r: %#v\n", r)
	fmt.Printf("m: %#v\n", m)
	fmt.Println(s)
	s[dst] = append(m, s[dst]...)

	fmt.Println(s)
}

func (s Stacks) String() string {
	result := ""
	for i, stack := range s {
		result += fmt.Sprintf("%d:", i)
		for _, crate := range stack {
			result += fmt.Sprintf(" [%s]", crate)
		}
		result += "\n"
	}
	return result
}

func LoadInput(input string) *Stacks {
	result := make(Stacks, 9)

	for i := range result {
		result[i] = make([]string, 0)
	}

	for _, line := range strings.Split(input, "\n") {
		if strings.HasPrefix(line, "[") {
			if string(line[1]) != " " {
				result[0] = append(result[0], string(line[1]))
			}
			if string(line[5]) != " " {
				result[1] = append(result[1], string(line[5]))
			}
			if string(line[9]) != " " {
				result[2] = append(result[2], string(line[9]))
			}
			if string(line[13]) != " " {
				result[3] = append(result[3], string(line[13]))
			}
			if string(line[17]) != " " {
				result[4] = append(result[4], string(line[17]))
			}
			if string(line[21]) != " " {
				result[5] = append(result[5], string(line[21]))
			}
			if string(line[25]) != " " {
				result[6] = append(result[6], string(line[25]))
			}
			if string(line[29]) != " " {
				result[7] = append(result[7], string(line[29]))
			}
			if string(line[33]) != " " {
				result[8] = append(result[8], string(line[33]))
			}
		}
	}

	return &result
}

func Event2022Day5Part1(task *aoc.Task) (string, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	s := LoadInput(task.Input)

	lines := strings.Split(task.Input, "\n")

	for _, line := range lines[10:514] {
		re, err := regexp.Compile(`move (\d+) from (\d+) to (\d+)`)
		if err != nil {
			log.Fatalf("Regex compile failed: %s", err.Error())
		}

		m := re.FindStringSubmatch(line)

		num, err := strconv.Atoi(m[1])
		if err != nil {
			log.Fatalf("Failed converting to int: %s", err.Error())
		}
		src, err := strconv.Atoi(m[2])
		if err != nil {
			log.Fatalf("Failed converting to int: %s", err.Error())
		}
		dst, err := strconv.Atoi(m[3])
		if err != nil {
			log.Fatalf("Failed converting to int: %s", err.Error())
		}

		s.MoveNum(num, src-1, dst-1)
	}

	result := ""
	for _, stack := range *s {
		result += stack[0]
	}

	return result, nil
}

func Event2022Day5Part2(task *aoc.Task) (string, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	s := LoadInput(task.Input)

	lines := strings.Split(task.Input, "\n")

	for _, line := range lines[10:514] {
		re, err := regexp.Compile(`move (\d+) from (\d+) to (\d+)`)
		if err != nil {
			log.Fatalf("Regex compile failed: %s", err.Error())
		}

		m := re.FindStringSubmatch(line)

		num, err := strconv.Atoi(m[1])
		if err != nil {
			log.Fatalf("Failed converting to int: %s", err.Error())
		}
		src, err := strconv.Atoi(m[2])
		if err != nil {
			log.Fatalf("Failed converting to int: %s", err.Error())
		}
		dst, err := strconv.Atoi(m[3])
		if err != nil {
			log.Fatalf("Failed converting to int: %s", err.Error())
		}

		s.MoveNum9001(num, src-1, dst-1)
	}

	result := ""
	for _, stack := range *s {
		result += stack[0]
	}

	return result, nil
}

func main() {

	// submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 5)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result string
	switch *part {
	case 1:
		result, err = Event2022Day5Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day5Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %s\n", result)

	// if *submit {
	// 	msg, err := mgr.SubmitResult(task, *part, result)
	// 	if err != nil {
	// 		log.Fatalf("Error: unable to submit result - %s", err.Error())
	// 	}

	// 	fmt.Println(msg)
	// }
}
