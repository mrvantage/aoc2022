package main

import (
	"flag"
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
	"gopkg.in/yaml.v3"
)

type Monkey struct {
	Items     []int
	Operation func(old int) int
	Test      struct {
		ConditionModulus int
		TrueMonkeyId     int
		TrueMonkey       *Monkey
		FalseMonkeyId    int
		FalseMonkey      *Monkey
	}
	NumInspections int
}

func (m *Monkey) UpdateMonkeyRelations(monkeys map[string]*Monkey) {
	if monkey, exists := monkeys[fmt.Sprintf("Monkey %d", m.Test.TrueMonkeyId)]; exists {
		m.Test.TrueMonkey = monkey
	}

	if monkey, exists := monkeys[fmt.Sprintf("Monkey %d", m.Test.FalseMonkeyId)]; exists {
		m.Test.FalseMonkey = monkey
	}
}

func (m *Monkey) Catch(item int) {
	m.Items = append(m.Items, item)
}

func (m *Monkey) KeepAwayRound() {

	for _, item := range m.Items {
		log.Printf("    Monkey inspects an item with a worry level of %d.", item)

		new := m.Operation(item)
		// new = int(math.Round(float64(new) / float64(3)))
		new = new / 3
		log.Printf("      Monkey gets bored with item. Worry level is divided by 3 to %d.", new)

		if new%m.Test.ConditionModulus == 0 {
			log.Printf("      Current worry level is divisible by %d.", m.Test.ConditionModulus)
			log.Printf("      Item with worry level %d is thrown to monkey %d.", new, m.Test.TrueMonkeyId)
			m.Test.TrueMonkey.Catch(new)
		} else {
			log.Printf("      Current worry level is not divisible by %d.", m.Test.ConditionModulus)
			log.Printf("      Item with worry level %d is thrown to monkey %d.", new, m.Test.FalseMonkeyId)
			m.Test.FalseMonkey.Catch(new)
		}

		m.NumInspections++
	}

	m.Items = make([]int, 0)
}

func (m *Monkey) UnmarshalYAML(value *yaml.Node) (err error) {

	v := struct {
		Items     string `yaml:"Starting items"`
		Operation string `yaml:"Operation"`
		Test      struct {
			ConditionModulusString string `yaml:"Condition"`
			TrueMonkeyString       string `yaml:"If true"`
			FalseMonkeyString      string `yaml:"If false"`
		} `yaml:"Test"`
	}{}

	value.Decode(&v)

	// log.Printf("%v", v)

	if m.Items == nil {
		m.Items = make([]int, 0)
	}
	parts := strings.Split(v.Items, ", ")
	for _, part := range parts {
		var num int
		_, err = fmt.Sscanf(part, "%d", &num)
		if err != nil {
			return
		}
		m.Items = append(m.Items, num)
	}

	var par1, op, par2 string
	_, err = fmt.Sscanf(v.Operation, "new = %s %s %s", &par1, &op, &par2)
	if err != nil {
		return
	}

	par2Int, err := strconv.Atoi(par2)
	if err == nil {
		if op == "*" {
			m.Operation = func(old int) (res int) {
				res = old * par2Int
				log.Printf("      Worry level is multiplied by %d to %d.", par2Int, res)
				return
			}
		} else if op == "+" {
			m.Operation = func(old int) (res int) {
				res = old + par2Int
				log.Printf("      Worry level increases by %d to %d.", par2Int, res)
				return
			}
		}
		err = nil
	} else {
		m.Operation = func(old int) (res int) {
			res = old * old
			log.Printf("      Worry level is multiplied by old %d to %d. par2 %s", old, res, par2)
			return
		}
	}

	var cond int
	_, err = fmt.Sscanf(v.Test.ConditionModulusString, "divisible by %d", &cond)
	if err != nil {
		return
	}

	m.Test.ConditionModulus = cond

	var trueId int
	_, err = fmt.Sscanf(v.Test.TrueMonkeyString, "throw to monkey %d", &trueId)
	if err != nil {
		return
	}

	m.Test.TrueMonkeyId = trueId

	var falseId int
	_, err = fmt.Sscanf(v.Test.FalseMonkeyString, "throw to monkey %d", &falseId)
	if err != nil {
		return
	}

	m.Test.FalseMonkeyId = falseId

	return
}

type BunchOfMonkeys map[string]*Monkey

func (b BunchOfMonkeys) UpdateMonkeyRelations() {
	for _, monkey := range b {
		monkey.UpdateMonkeyRelations(b)
	}
}

func (b BunchOfMonkeys) KeepAwayRound() {

	keys := make([]string, 0)
	for k := range b {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		log.Printf("  %s:", k)
		b[k].KeepAwayRound()
	}
}

func (b BunchOfMonkeys) KeepAwayRounds(num int) {
	for i := 0; i < num; i++ {
		b.KeepAwayRound()
	}
}

func (b BunchOfMonkeys) GetMonkeyBusiness() (monkey1, monkey2 *Monkey) {
	var num1, num2 int

	keys := make([]string, 0)
	for k := range b {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {

		log.Printf("Business for monkey `%s` is %d", k, b[k].NumInspections)

		if b[k].NumInspections > num1 {
			if num1 >= num2 {
				monkey2 = monkey1
				num2 = monkey2.NumInspections
			}
			monkey1 = b[k]
			num1 = monkey1.NumInspections
		} else if b[k].NumInspections > num2 {
			monkey2 = monkey1
			num2 = monkey2.NumInspections
		}
	}

	log.Printf("Busiest monkey1 has %d inspections", monkey1.NumInspections)
	log.Printf("Busiest monkey2 has %d inspections", monkey2.NumInspections)
	return
}

func (b BunchOfMonkeys) GetMonkeyBusiness2() (monkey1, monkey2 *Monkey) {

	keys := make([]string, 0, len(b))

	for key := range b {
		keys = append(keys, key)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return b[keys[i]].NumInspections > b[keys[j]].NumInspections
	})

	for _, k := range keys {
		log.Printf("Business for monkey `%s` is %d", k, b[k].NumInspections)
	}

	monkey1 = b[keys[0]]
	monkey2 = b[keys[1]]
	return
}

func Event2022Day11Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	input := task.Input

	input = strings.ReplaceAll(input, "Test: ", "Test:\n    Condition: ")

	// fmt.Println(input)

	b := make(BunchOfMonkeys)
	err := yaml.Unmarshal([]byte(input), &b)
	if err != nil {
		log.Default().Fatalf("failed parsing yaml: %s", err.Error())
	}

	b.UpdateMonkeyRelations()
	b.KeepAwayRounds(20)
	monkey1, monkey2 := b.GetMonkeyBusiness2()

	return monkey1.NumInspections * monkey2.NumInspections, nil
}

func Event2022Day11Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	/*
		Write the solution to the AoC problem part 2 here
	*/

	return 0, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 11)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day11Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day11Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
