package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/mrvantage/aoc"
)

func Event2022Day6Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	input := task.Input

	var result int
	for i := range input {

		if i > 3 {
			if input[i] != input[i-1] && input[i] != input[i-2] && input[i] != input[i-3] {
				if input[i-1] != input[i-2] && input[i-1] != input[i-3] {
					if input[i-2] != input[i-3] {

						// log.Printf("%d: %v %v %v %v", i, input[i], input[i-1], input[i-2], input[i-3])
						result = i
						break
					}
				}
			}
		}
	}

	return result + 1, nil

}

func hasDuplicates(input string) bool {
	checkMap := make(map[rune]int)

	for _, b := range input {
		if checkMap[b] > 0 {
			return true
		} else {
			checkMap[b]++
		}
	}

	return false
}

func Event2022Day6Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	input := task.Input

	var result int
	for i := range input {
		if i > 13 {
			if !hasDuplicates(input[i-13 : i+1]) {
				result = i
				break
			}
		}
	}
	return result + 1, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 6)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day6Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day6Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
