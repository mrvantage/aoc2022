package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

func Event2022Day4Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	var count int
	for _, pair := range strings.Split(task.Input, "\n") {

		if len(pair) > 0 {
			assignments := strings.Split(pair, ",")

			elf1 := strings.Split(assignments[0], "-")
			elf2 := strings.Split(assignments[1], "-")

			elf1Start, _ := strconv.Atoi(elf1[0])
			elf1End, _ := strconv.Atoi(elf1[1])
			elf2Start, _ := strconv.Atoi(elf2[0])
			elf2End, _ := strconv.Atoi(elf2[1])

			if elf1Start >= elf2Start && elf1End <= elf2End {
				count++
			} else if elf2Start >= elf1Start && elf2End <= elf1End {
				count++
			}

		}

	}

	return count, nil
}

func Event2022Day4Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	var count int
	for _, pair := range strings.Split(task.Input, "\n") {

		if len(pair) > 0 {
			assignments := strings.Split(pair, ",")

			elf1 := strings.Split(assignments[0], "-")
			elf2 := strings.Split(assignments[1], "-")

			elf1Start, _ := strconv.Atoi(elf1[0])
			elf1End, _ := strconv.Atoi(elf1[1])
			elf2Start, _ := strconv.Atoi(elf2[0])
			elf2End, _ := strconv.Atoi(elf2[1])

			if elf1Start >= elf2Start && elf1Start <= elf2End {
				count++
			} else if elf1End >= elf2Start && elf1End <= elf2End {
				count++
			} else if elf2Start >= elf1Start && elf2Start <= elf1End {
				count++
			} else if elf2End >= elf1Start && elf2End <= elf1End {
				count++
			}

		}

	}

	return count, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 4)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day4Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day4Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
