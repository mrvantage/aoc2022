package main

import (
	"flag"
	"fmt"
	"log"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

func Event2022Day10Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	input := task.Input

	var regX int = 1
	var cycle int = 1
	var signalStrengthSum int

	cyclesOut := []int{
		20,
		60,
		100,
		140,
		180,
		220,
	}

	for _, line := range strings.Split(input, "\n") {

		if len(line) == 0 {
			continue
		}

		var instruction string
		var val int
		fmt.Sscanf(line, "%s %d", &instruction, &val)

		switch instruction {
		case "noop":
			cycle++

			if len(cyclesOut) > 0 && cycle >= cyclesOut[0] {
				signalStrengthSum += cycle * regX
				cyclesOut = append(make([]int, 0), cyclesOut[1:]...)
			}

		case "addx":
			cycle++

			if len(cyclesOut) > 0 && cycle >= cyclesOut[0] {
				signalStrengthSum += cycle * regX
				cyclesOut = append(make([]int, 0), cyclesOut[1:]...)
			}

			cycle++
			regX += val

			if len(cyclesOut) > 0 && cycle >= cyclesOut[0] {
				signalStrengthSum += cycle * regX
				cyclesOut = append(make([]int, 0), cyclesOut[1:]...)
			}
		}

	}

	return signalStrengthSum, nil
}

func Event2022Day10Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	input := task.Input

	var regX int = 1
	var cycle int = 1
	var row int
	var output string

	for _, line := range strings.Split(input, "\n") {

		if len(line) == 0 {
			continue
		}

		var instruction string
		var val int
		fmt.Sscanf(line, "%s %d", &instruction, &val)

		switch instruction {
		case "noop":
			log.Printf("%d: running noop", cycle)

			pixel := (cycle % 40) - 1

			if pixel > regX-2 && pixel < regX+2 {
				log.Printf("%d: pixel %d:%d on (%d)", cycle, row, pixel, regX)
				output += "#"
			} else {
				log.Printf("%d: pixel %d:%d off (%d)", cycle, row, pixel, regX)
				output += "."
			}

			if cycle%40 == 0 {
				output += "\n"
				row++
			}

			cycle++

		case "addx":
			log.Printf("%d: running addx %d", cycle, val)

			pixel := (cycle % 40) - 1

			if pixel > regX-2 && pixel < regX+2 {
				log.Printf("%d: pixel %d:%d on (%d)", cycle, row, pixel, regX)
				output += "#"
			} else {
				log.Printf("%d: pixel %d:%d off (%d)", cycle, row, pixel, regX)
				output += "."
			}

			if cycle%40 == 0 {
				output += "\n"
				row++
			}

			cycle++

			pixel = (cycle % 40) - 1

			if pixel > regX-2 && pixel < regX+2 {
				log.Printf("%d: pixel %d:%d on (%d)", cycle, row, pixel, regX)
				output += "#"
			} else {
				log.Printf("%d: pixel %d:%d off (%d)", cycle, row, pixel, regX)
				output += "."
			}

			if cycle%40 == 0 {
				output += "\n"
				row++
			}

			cycle++
			regX += val
		}

	}

	fmt.Println(output)

	return 0, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 10)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day10Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day10Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
