package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

type LocationGiver interface {
	GetLocation() Location
}

type Location struct {
	X int
	Y int
}

func (l Location) GetLocation() Location { return l }

func (l Location) String() string {
	return fmt.Sprintf("(%d,%d)", l.X, l.Y)
}

type LocationMap map[int]map[int]int

func (l LocationMap) IncrementLocation(loc Location) *LocationMap {
	if l[loc.X] == nil {
		l[loc.X] = make(map[int]int, 0)
	}
	l[loc.X][loc.Y]++
	return &l
}

func (l LocationMap) NumUsedLocations() (res int) {
	for _, x := range l {
		for _, val := range x {
			if val > 0 {
				res++
			}
		}
	}
	return
}

type Head struct {
	Location
	Follower *Knot
}

func (h *Head) Move(direction string) {
	switch direction {
	case "U":
		h.Y++
	case "D":
		h.Y--
	case "L":
		h.X--
	case "R":
		h.X++
	}
	log.Printf("Head is now at %s", h.Location)

	if h.Follower != nil {
		h.Follower.Update()
	}
}

func (h *Head) NumUsedLocations() int {
	return h.Follower.NumUsedLocations()
}

func (h *Head) NumUsedLocationsLastKnot() int {
	return h.Follower.NumUsedLocationsLastKnot()
}

type Knot struct {
	Num int
	Location
	Follower *Knot
	Follows  LocationGiver
	Visited  *LocationMap
}

// . x x x .
// x . . . x
// x . H . x
// X . . . x
// . X x x .

func (k *Knot) Update() {

	f := k.Follows.GetLocation()

	if Abs(f.Y-k.Y) > 1 && Abs(f.X-k.X) > 1 {

		if f.X > k.X {
			k.X = f.X - 1
		} else {
			k.X = f.X + 1
		}

		if f.Y > k.Y {
			k.Y = f.Y - 1
		} else {
			k.Y = f.Y + 1
		}

	} else if Abs(f.Y-k.Y) > 1 {
		k.X = f.X
		if f.Y > k.Y {
			k.Y = f.Y - 1
		} else {
			k.Y = f.Y + 1
		}

	} else if Abs(f.X-k.X) > 1 {
		k.Y = f.Y
		if f.X > k.X {
			k.X = f.X - 1
		} else {
			k.X = f.X + 1
		}

	}

	k.Visited = k.Visited.IncrementLocation(k.Location)

	log.Printf("Knot %d is now at %s", k.Num, k.Location)

	if k.Follower != nil {
		k.Follower.Update()
	}
}

func (k *Knot) NumUsedLocations() (res int) {
	res += k.Visited.NumUsedLocations()
	if k.Follower != nil {
		res += k.Follower.NumUsedLocations()
	}
	return
}

func (k *Knot) NumUsedLocationsLastKnot() (res int) {
	log.Printf("Getting NumUsedLocationsLastKnot for knot %d", k.Num)
	if k.Follower == nil {
		res = k.NumUsedLocations()
		log.Printf("found NumUsedLocationsLastKnot for knot %d", k.Num)
	} else {
		res = k.Follower.NumUsedLocationsLastKnot()
	}
	return
}

type Rope struct {
	Input string
	Head  *Head
}

func (r *Rope) MoveNum(direction string, num int) {
	for i := 0; i < num; i++ {
		r.Head.Move(direction)
	}
}

func (r *Rope) ApplyInput() {
	for _, line := range strings.Split(r.Input, "\n") {
		if len(line) > 0 {
			num, _ := strconv.Atoi(string(line[2:]))
			direction := string(line[0])

			r.MoveNum(direction, num)
		}
	}
}

func (r *Rope) NumUsedLocations() int {
	return r.Head.NumUsedLocations()
}

func (r *Rope) NumUsedLocationsLastKnot() int {
	return r.Head.NumUsedLocationsLastKnot()
}

func NewRopeWithNumKnots(input string, numKnots int) *Rope {

	h := &Head{}

	k := make([]*Knot, numKnots)

	for i := 0; i < numKnots; i++ {

		v := make(LocationMap, 0)

		var f LocationGiver
		if i == 0 {
			f = h
		} else {
			f = k[i-1]
		}
		k[i] = &Knot{Num: i, Visited: &v, Follows: f}
	}

	for i := 0; i < numKnots-1; i++ {
		k[i].Follower = k[i+1]
	}

	h.Follower = k[0]

	return &Rope{Input: input, Head: h}

}

func Event2022Day9Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	input := `
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
`
	input = task.Input

	r := NewRopeWithNumKnots(input, 1)

	r.ApplyInput()

	return r.NumUsedLocations(), nil
}

func Event2022Day9Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	input := `
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
`

	input = `
R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
`
	input = task.Input

	r := NewRopeWithNumKnots(input, 9)

	r.ApplyInput()

	return r.NumUsedLocationsLastKnot(), nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 9)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day9Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day9Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
