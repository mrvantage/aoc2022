package main

import (
	"testing"
)

func TestRope_NumUsedLocations(t *testing.T) {
	type fields struct {
		Input string
		Knots int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "example",
			fields: fields{
				Input: `R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
`,
				Knots: 1,
			},
			want: 13,
		},
		{
			name: "test vertical",
			fields: fields{
				Input: `D 4
U 6
`,
				Knots: 1,
			},
			want: 5,
		},
		{
			name: "test horizontal",
			fields: fields{
				Input: `R 4
L 6
`,
				Knots: 1,
			},
			want: 5,
		},
		{
			name: "test diagonal NW1",
			fields: fields{
				Input: `
L 1
U 1
L 1
U 1
L 1
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal NW2",
			fields: fields{
				Input: `
L 1
U 2
L 2
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal NE1",
			fields: fields{
				Input: `
R 1
U 1
R 1
U 1
R 1
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal NE2",
			fields: fields{
				Input: `
R 1
U 2
R 2
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal SE1",
			fields: fields{
				Input: `
R 1
D 1
R 1
D 1
R 1
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal SE2",
			fields: fields{
				Input: `
R 1
D 2
R 2
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal SW1",
			fields: fields{
				Input: `
L 1
D 1
L 1
D 1
L 1
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal SW2",
			fields: fields{
				Input: `
L 1
D 2
L 2
`,
				Knots: 1,
			},
			want: 3,
		},
		{
			name: "test diagonal SW-NW",
			fields: fields{
				Input: `
L 1
D 2
L 2
D 2
L 2
U 2
L 2
U 2
L 2
`,
				Knots: 1,
			},
			want: 9,
		},
		{
			name: "circle round",
			fields: fields{
				Input: `
D 1
L 1
U 2
R 2
D 2
L 1
U 1
`,
				Knots: 1,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tr := NewRopeWithNumKnots(tt.fields.Input, tt.fields.Knots)

			tr.ApplyInput()

			if got := tr.NumUsedLocations(); got != tt.want {
				t.Errorf("Tail.NumUsedLocations() = %v, want %v", got, tt.want)
			}
		})
	}
}
