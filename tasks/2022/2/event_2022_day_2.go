package main

import (
	"flag"
	"fmt"
	"log"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

func Event2022Day2Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	var score int
	for _, game := range strings.Split(task.Input, "\n") {

		switch game {
		case "A X":
			score += 1 + 3
		case "A Y":
			score += 2 + 6
		case "A Z":
			score += 3 + 0
		case "B X":
			score += 1 + 0
		case "B Y":
			score += 2 + 3
		case "B Z":
			score += 3 + 6
		case "C X":
			score += 1 + 6
		case "C Y":
			score += 2 + 0
		case "C Z":
			score += 3 + 3
		}
	}

	return score, nil
}

func Event2022Day2Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	var score int
	for _, game := range strings.Split(task.Input, "\n") {

		switch game {
		case "A X":
			score += 3 + 0
		case "A Y":
			score += 1 + 3
		case "A Z":
			score += 2 + 6
		case "B X":
			score += 1 + 0
		case "B Y":
			score += 2 + 3
		case "B Z":
			score += 3 + 6
		case "C X":
			score += 2 + 0
		case "C Y":
			score += 3 + 3
		case "C Z":
			score += 1 + 6
		}
	}

	return score, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 2)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day2Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day2Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
