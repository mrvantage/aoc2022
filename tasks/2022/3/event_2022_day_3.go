package main

import (
	"flag"
	"fmt"
	"log"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

func Event2022Day3Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	items := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	scores := make(map[rune]int, 0)
	for i, item := range items {
		scores[item] = i + 1
	}

	var sum int
	for _, rucksack := range strings.Split(task.Input, "\n") {

		if len(rucksack) > 1 {

			// size := int(math.Round(float64(len(rucksack)) / float64(2)))
			compartmentSize := len(rucksack) / 2

			compartment1 := rucksack[0:compartmentSize]
			compartment2 := rucksack[compartmentSize:]

			for _, item := range compartment2 {
				if strings.ContainsRune(compartment1, item) {
					sum += scores[item]
					break
				}
			}

		}
	}

	return sum, nil
}

func Event2022Day3Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	items := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	scores := make(map[rune]int, 0)
	for i, item := range items {
		scores[item] = i + 1
	}

	end := 2

	lines := strings.Split(task.Input, "\n")

	var sum int
	for len(lines) > end {
		for _, item := range lines[end] {
			if strings.ContainsRune(lines[end-1], item) && strings.ContainsRune(lines[end-2], item) {
				sum += scores[item]
				break
			}
		}

		end += 3
	}

	return sum, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 3)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day3Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day3Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
