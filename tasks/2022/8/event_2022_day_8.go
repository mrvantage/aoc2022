package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

type Grid [][]int

func (g Grid) String() string {
	result := ""

	for y, row := range g {
		line := ""
		for x := range row {
			line += fmt.Sprintf("%d", g[y][x])
		}
		result += line + "\n"
	}
	return result
}

func (g Grid) StringVisible() string {
	result := ""

	for y, row := range g {
		line := ""
		for x := range row {
			if g.TreeIsVisible(y, x) {
				line += fmt.Sprintf("%d", g[y][x])
			} else {
				line += " "
			}
		}
		result += line + "\n"
	}
	return result
}

func (g Grid) StringScenicScore() string {
	result := ""

	for y, row := range g {
		line := ""
		for x := range row {
			score := g.TreesScenicScore(y, x) / 50000
			if score > 0 {
				line += fmt.Sprintf("%d", score)
			} else {
				line += "."
			}
		}
		result += line + "\n"
	}
	return result
}

func (g Grid) ParseAddLine(line string) (*Grid, error) {
	if len(line) == 0 {
		return &g, nil
	}

	row := make([]int, 0)
	for _, c := range line {

		val, err := strconv.Atoi(string(c))
		if err != nil {
			return &g, err
		}

		row = append(row, val)
	}

	res := append(g, row)
	return &res, nil
}

func (g Grid) Width() int {
	return len(g[0])
}

func (g Grid) Height() int {
	return len(g)
}

func (g Grid) TreeIsVisibleFromTop(y, x int) (res bool) {
	res = true
	for i := y - 1; i >= 0; i-- {
		if g[i][x] >= g[y][x] {
			res = false
		}
	}
	return
}

func (g Grid) TreeIsVisibleFromRight(y, x int) (res bool) {
	res = true
	for i := x + 1; i < g.Width(); i++ {
		if g[y][i] >= g[y][x] {
			res = false
		}
	}
	return
}

func (g Grid) TreeIsVisibleFromBottom(y, x int) (res bool) {
	res = true
	for i := y + 1; i < g.Height(); i++ {
		if g[i][x] >= g[y][x] {
			res = false
		}
	}
	return
}

func (g Grid) TreeIsVisibleFromLeft(y, x int) (res bool) {
	res = true
	for i := x - 1; i >= 0; i-- {
		if g[y][i] >= g[y][x] {
			res = false
		}
	}
	return
}

func (g Grid) TreeIsVisible(y, x int) (res bool) {
	if g.TreeIsVisibleFromTop(y, x) || g.TreeIsVisibleFromRight(y, x) || g.TreeIsVisibleFromBottom(y, x) || g.TreeIsVisibleFromLeft(y, x) {
		res = true
	}
	return
}

func (g Grid) CountVisible() (res int) {
	for y, row := range g {
		for x := range row {
			if g.TreeIsVisible(y, x) {
				res++
			}
		}
	}
	return
}

func (g Grid) TreesVisibleToTop(y, x int) (res int) {
	for i := y - 1; i >= 0; i-- {
		res++
		if g[i][x] >= g[y][x] {
			return
		}
	}
	return
}

func (g Grid) TreesVisibleToRight(y, x int) (res int) {
	for i := x + 1; i < g.Width(); i++ {
		res++
		if g[y][i] >= g[y][x] {
			return
		}
	}
	return
}

func (g Grid) TreesVisibleToBottom(y, x int) (res int) {
	for i := y + 1; i < g.Height(); i++ {
		res++
		if g[i][x] >= g[y][x] {
			return
		}
	}
	return
}

func (g Grid) TreesVisibleToLeft(y, x int) (res int) {
	for i := x - 1; i >= 0; i-- {
		res++
		if g[y][i] >= g[y][x] {
			return
		}
	}
	return
}

func (g Grid) TreesScenicScore(y, x int) int {
	top := g.TreesVisibleToTop(y, x)
	right := g.TreesVisibleToRight(y, x)
	bottom := g.TreesVisibleToBottom(y, x)
	left := g.TreesVisibleToLeft(y, x)
	return top * right * bottom * left
}

func (g Grid) MaxScenicScore() (res, resy, resx int) {

	for y, row := range g {
		for x := range row {
			score := g.TreesScenicScore(y, x)
			if score > res {
				res = score
				resy = y
				resx = x
			}
		}
	}
	return
}

func NewGrid() *Grid {
	g := make(Grid, 0)
	return &g
}

func Event2022Day8Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	g := NewGrid()

	var err error
	for _, line := range strings.Split(task.Input, "\n") {
		g, err = g.ParseAddLine(line)
		if err != nil {
			log.Fatalf("parsing failed: %s", err.Error())
		}
	}

	fmt.Println(g)
	fmt.Println(g.StringVisible())

	return g.CountVisible(), nil
}

func Event2022Day8Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	g := NewGrid()

	var err error
	for _, line := range strings.Split(task.Input, "\n") {
		g, err = g.ParseAddLine(line)
		if err != nil {
			log.Fatalf("parsing failed: %s", err.Error())
		}
	}

	fmt.Println(g)
	fmt.Println(g.StringScenicScore())

	score, _, _ := g.MaxScenicScore()

	return score, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 8)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day8Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day8Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
