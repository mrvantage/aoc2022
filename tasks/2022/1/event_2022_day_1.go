package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc"
)

func Event2022Day1Part1(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	max := 0
	for elf, elfList := range strings.Split(task.Input, "\n\n") {
		num := 0

		log.Printf("Parsing elf %d", elf)

		for _, item := range strings.Split(elfList, "\n") {
			if cal, err := strconv.Atoi(item); err == nil {
				num += cal
			}
		}

		log.Printf("Num for elf %d is %d", elf, num)

		if num > max {
			max = num
		}
	}

	return max, nil
}

func Event2022Day1Part2(task *aoc.Task) (int, error) {
	log.Printf("Length of task input is %d", len(task.Input))

	top := make([]int, 3)

	for elf, elfList := range strings.Split(task.Input, "\n\n") {
		num := 0

		log.Printf("Parsing elf %d", elf)

		for _, item := range strings.Split(elfList, "\n") {
			if cal, err := strconv.Atoi(item); err == nil {
				num += cal
			}
		}

		log.Printf("Num for elf %d is %d", elf, num)

		for key, topItem := range top {
			if num > topItem {
				top[key] = num
				break
			}
		}
	}

	total := 0
	for _, topItem := range top {
		total += topItem
	}

	return total, nil
}

func main() {

	submit := flag.Bool("submit", false, "Directly submit the result to AoC")
	part := flag.Int("part", 1, "Part to execute")
	flag.Parse()

	mgr, err := aoc.NewManager()
	if err != nil {
		log.Fatalf("Error: unable to create manager - %s", err.Error())
	}

	task, err := mgr.GetTask(2022, 1)
	if err != nil {
		log.Fatalf("Error: unable to get task - %s", err.Error())
	}

	var result int
	switch *part {
	case 1:
		result, err = Event2022Day1Part1(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	case 2:
		result, err = Event2022Day1Part2(task)
		if err != nil {
			log.Fatalf("Error: unable to get task - %s", err.Error())
		}
	default:
		log.Fatalf("unknown part %d", part)
	}

	fmt.Printf("Result = %d\n", result)

	if *submit {
		msg, err := mgr.SubmitResult(task, *part, result)
		if err != nil {
			log.Fatalf("Error: unable to submit result - %s", err.Error())
		}

		fmt.Println(msg)
	}
}
